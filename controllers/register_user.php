<?php

	require "./connection.php";
	// php has predefined variables which are designed to collect the data sent by HTML form which $_POST
	//  and $_GET superglobal variables
	

	// superglobal variable simply means that it is a specially predefined variable(normally arrays) that can 
	// be accessed in the program

	// the request returned an array where it established the name attributeof the input as the key and the value value inputted to it as its value.
	// var_dump($_POST);
	// var_dump($_POST['firstName']);
	// var_dump($_GET);


	// POST and GET do the same thingas both variables handle html form data but the main difference is when you use the GET method,  the query string we entered in the form will be displayed in the URL. POST on the other hand sends forms behind the scenes. thus not seeing the form data in the URL.

	//sanitize our inputs
	$fname = htmlspecialchars($_POST['firstName']);
    $lname = htmlspecialchars($_POST['lastName']);
    $username = htmlspecialchars($_POST['username']);
    $address = htmlspecialchars($_POST['address']);
    $email = htmlspecialchars($_POST['email']);
    $password = htmlspecialchars($_POST['password']);
    $confirmPassword = htmlspecialchars($_POST['confirmPassword']);
    $role_id = 1;
	// to check the values use var_dump which is the equivalent of console.log in javascript.

	// var_dump($fname);
	// var_dump($lname);
	// var_dump($email);
	// var_dump($password);
	// var_dump($confirmPassword);

	if($password != "" || $confirmPassword != ""){  // this means that if it has no assignment.
		// lets hash our password to make it secure.
		$password = sha1($password);
		$confirmPassword = sha1($confirmPassword);
		// var_dump($password);
		// var_dump($confirmPassword);
		// check if password is equal to confirm password.

		if($password == $confirmPassword) { 
			// echo "passwords matched";
			//retrieve the contents of accounts.json
			//file_get_contents() will return the content in a string.
			//file_get_contents(filename that we want to extract)


			$insert_query = "INSERT INTO users(username, password, firstname, lastname, email, address, role_id) VALUES(\"$username\", \"$password\", \"$fname\", \"$lname\", \"$email\", \"$address\", $role_id)";
 
            $result = mysqli_query($conn, $insert_query);
 
            if($result){
                echo "Registered Successfully...";
            }
            else{
                echo mysqli_error($conn);
            }
       
            // header('Location: ../views/login.php')


		} else { 

			echo "passwords did not match";	
		}
		// we are going to be storing to a JSON file. JSON stands for Javascript Object Notation. it is used in exchanging and storing data from the web sever. JSON uses the object notation/syntax of Javascript.



	} else { 
		echo "Please check the password fields";

	}

	if($fname != "" && $lname != ""){ 

		echo "<br> Welcome " . $fname . " " . $lname;

	} else { 
		echo "please provide a complete name";

	}

	// if $email input is not equal to an empty string, echo out 
	// your email is: (value of the email variable)\
	// else, echo out please provide an email
	if($email != "") { 
		echo "<br>your email is: " . $email;

	} else { 
		echo "please provide a valid email";

	}

?>