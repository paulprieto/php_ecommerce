<?php 
	require_once '../partials/header.php';
	function getTitle(){
		return "Login Page";
	}

	// var_dump($_GET);
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
			<div class="card">
				<img src="<?= $_GET['image'] ?>" class="card-img-top" >
				<div class="card-body">
					<h5 class="card-title"><?= $_GET['name'] ?></h5>
					<p class="card-text"><?= $_GET['price'] ?></p>
					<p class="card-text"><?= $_GET['description'] ?></p>
				</div>
			</div>
		</div>

		<div class="col-md-9">	
			<div class="row">
				<div class="col-md-12 text-center mx-auto">
					<h2><?= $_GET['name'] ?></h2>
					<!-- <p>
						<?= $_GET['description'] ?>
					</p> -->
				</div>
			</div>

			<div class="row">
				<form class="col-md-12" action="#" method="#">
					<label for="numberOfItem">Quantity: </label>
					<input type="number" name="numberOfItem" class="col-3">


					<button type="submit">Confirm</button>
				</form>
			</div>
		</div>

	</div>
</div>



<?php 
	require_once '../partials/footer.php';
?>