<?php 
	require_once '../partials/header.php';

	function getTitle(){
		return 'My cart';
	}
?>




<div class="container-fluid">
	<h2 class="text-center">My Cart </h2>
	<div class="row">
		<div class="col-md-10 text-center mx-auto">
			<a href="./gallery.php"><h5>Add More Items</h5></a>
		</div>

	</div>
	<div class="row">
		<div class="col-md-10 mx-auto">
			<div class="table-responsive" >
				<table class="table table-striped table table-bordered" id="cart-items" >

					<thead>
						<tr>
							<th>Item</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php 
							$total =0;
							// var_dump($_SESSION['cart']);
							if (isset($_SESSION['cart']) && count($_SESSION['cart']) !=0) {
								# code...
								foreach ($_SESSION['cart'] as $item_id => $item_quantity) {
								# code...
								// var_dump($item_id);
								
								$item_query	= "SELECT * FROM items WHERE id = $item_id";

								$result = mysqli_query($conn, $item_query);
								$item = mysqli_fetch_assoc($result);
								// var_dump($item['name']);

								// convert the assoc array into set of variables w/ associative array keys
								extract($item);

								// echo $description;
								$subtotal = $price * $item_quantity;
								$total += $subtotal; 
							
						?>
						<tr>
							<td><?= $name; ?></td>
							<td>PHP <?= number_format($price); ?></td>
							<td>
								<!-- Item Quantity Data -->
								<form action="../controllers/update_cart.php" method="POST">
									<input type="number" name="item_quantity" class="quantityInput" value="<?= $item_quantity; ?>">
									<input type="hidden" name="item_id" value="<?= $item_id; ?>">
									<input type="hidden" name="updateFromCart" value="true">
									<button class="btn btn-primary">Update Quantity</button>
								</form>
							</td>


							<td>PHP <?= number_format($subtotal); ?></td>
							<td>
							<form action="../controllers/remove_from_cart.php" method="POST">
								<input type="hidden" name="item_id" value="<?= $item_id; ?>">
								<button class="btn btn-danger btn-block waves-effect waves-light">Remove From Cart</button></td>
							</form>

								
						</tr>


						<?php 
								} //end of for each
						?>
								<tr>
									<td></td>
									<td></td>
									<td>Total: PHP <?= number_format($total); ?></td>
									<td><a href="../controllers/empty_cart.php" class="btn btn-outline-danger btn-block waves-effect waves-light">Empty Cart</a></td>
									<td><a href="../controllers/checkout.php" class="btn btn-outline-success btn-block waves-effect waves-light">Checkout</a></td>
								</tr>
						<?php
							} //end of $_SESSION CART if statement
							else{
						?>

							<tr>
								<td colspan="5">No items in cart.</td>
							</tr>
							

						<?php 
							} //end of else 
						?>


						

					</tbody>

				</table>

			</div>

		</div>

	</div>

</div>


<?php  
	require_once '../partials/footer.php';
?>

