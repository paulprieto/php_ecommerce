<?php 
	require_once '../partials/header.php';

	function getTitle(){
		return 'Update Item';
	}

	$edit_id = $_GET['id'];

	// var_dump($edit);

	$item_query = "SELECT * FROM items WHERE id = $edit_id";
	$item_result = mysqli_query($conn, $item_query);

	$item = mysqli_fetch_assoc($item_result);



?>


<div id="body" class="container-fluid">
	<h2 class="text-center">Update Item</h2>

	<div class="row">
		<div class="col-md-10 mx-auto">
			<form action="../controllers/update_item.php" method="POST" enctype="multipart/form-data" >
				<div class="form-group">
					<label for="productName">Product Name</label>
					<input type="text" id="productName" name="productName" class="form-control"
					value="<?= $item['name'] ?>">
				</div>

				<div class="form-group">
					<label for="price">Price</label>
					<input type="number" id="price" name="price" class="form-control"
					value="<?= $item['price'] ?>">
				</div>

				<div class="form-group">
					<label for="description">Description</label>
					<input type="text" id="description" name="description" class="form-control materialize-textarea" value="<?= $item['description'] ?>">
				</div>

				<input type="hidden" name="edit_id" value="<?= $item['id']?>">

				<button type="submit" class="btn btn-primary waves-effect waves-light">Update<i class="material-icons right"></i></button>

			</form>
		</div>

	</div>

</div>

<?php  
	require_once '../partials/footer.php';
?>