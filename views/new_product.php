<?php 
	require_once '../partials/header.php';

	function getTitle(){
		return "Add Item Page";
	}

?>

<div id="body" class="container-fluid">
	<h2 class="text-center">Add New Item</h2>

	<div class="row">
		<div class="col-md-8 mx-auto">
			<form action="../controllers/add_item.php" method="POST" enctype="multipart/form-data" >
				<div class="form-group">
					<label for="productName">Product Name</label>
					<input type="text" id="productName" name="productName" class="form-control">
				</div>

				<div class="form-group">
					<label for="price">Price</label>
					<input type="number" id="price" name="price" class="form-control">
				</div>

				<div class="form-group">
					<label for="description">Description</label>
					<input type="text" id="description" name="description" class="form-control materialize-textarea">
				</div>

				<div class="form-group">
					<label for="image">Image</label>
					<input type="file" id="image" name="image" class="form-control">
				</div>
				<div class="form-group">
					<label for="category">Category</label>
					<select id="category" name="category" class="form-control">

						<!-- while category: print option -->
						<?php
					        $category_query = "SELECT * FROM categories";
					        // var_dump($product_query);
					        $categories_array = mysqli_query($conn, $category_query);
					        // var_dump($products_array);

					        foreach($categories_array as $category){
					        //var_dump($product);

					    ?>
						<option value="<?=  $category['id']?>"><?php echo $category['name']; ?></option>

						<?php 
							}
						?>

					</select>
				</div>


				<button type="submit" class="btn btn-primary waves-effect waves-light">Add New Item<i class="material-icons right"></i></button>

			</form>
		</div>

	</div>

</div>




<?php 
	require_once '../partials/footer.php' 
?>