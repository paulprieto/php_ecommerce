<?php 
	
	// the require statement is the same as the include statement in the sense that it allows devs to reference files from a different location. The only difference is the error handling.

	// include will throw a warning but still execute the code while require will throw a fatal error. include once and require once does the same thing but once the file is included already, it will not execute.


	// include '../partials/header.php';
	// include_once '../partials/header.php';
	// require_once '../partials/header.php';

	
	require_once '../partials/header.php';

	function getTitle(){
		return "Register Page";
	}
?>



<div id="body" class="container-fluid">
	<h2 class="text-center mt-4">Registration Page</h2>
	<div class="row">
		<div class="col-md-8 mx-auto">

			<!-- action attribute sets the destination to which the form data is submitted. thevalue of this can be an absolute or relative url -->

			<!-- the method attribute specifies the type of http request you want to make when sending the form data -->

			<form action="../controllers/register_user.php" method="POST">
				<div class="form-group  input-field">
					<label for="fname">First Name</label>
					<input type="text" id="fname" name="firstName" class="form-control">
				</div>

				<div class="form-group input-field">
					<label for="lname">Last Name</label>
					<input type="text" id="lname" name="lastName" class="form-control">
				</div>

				<div class="form-group input-field">
					<label for="email">Email</label>
					<input type="email" id="email" name="email" class="form-control">
				</div>

				<div class="form-group input-field">
					<label for="username">Username</label>
					<input type="text" id="username" name="username" class="form-control">
				</div>

				<div class="form-group input-field">
					<label for="address">Address</label>
					<input type="text" id="address" name="address" class="form-control">
				</div>


				<div class="form-group input-field">
					<label for="password">Password</label>
					<input type="password" id="password" name="password" class="form-control">
				</div>

				<div class="form-group input-field">
					<label for="confirm">Confirm Password</label>
					<input type="password" id="confirm" name="confirmPassword" class="form-control">
				</div>

				<!-- type submit will submit the form -->
				<button type="submit" class="btn btn-primary btn-block">Register</button>
				

			</form>
		</div>
	</div>
</div>

<?php 
	require "../partials/footer.php";
?>



    
    
    
 