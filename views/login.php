<?php 

	require_once '../partials/header.php';
	function getTitle(){
		return "Login Page";
	}
?>

<div id="body" class="container is-fluid">
	<h2 class="text-center">Login Page</h2>

	<div class="row">
		<div class="col-md-8 mx-auto">
			<form action="../controllers/authenticate.php" method="POST">
				<div class="form-group input-field">
					<label for="email" class="waves-effect waves">Email</label>
					<input type="email" name="email" id="email" class="form-control">
				</div>

				<div class="form-group input-field">
					<label for="password" class="waves-effect waves">Password</label>
					<input type="password" name="password" id="password" class="form-control">
				</div>

				<button type="submit" class="btn btn-primary btn-block mb-5 waves-effect waves">Login</button>

			</form>
		</div>
	</div>
</div>



<?php 
	require_once '../partials/footer.php';
?>