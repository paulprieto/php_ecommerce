<?php 
	
	require_once '../partials/header.php';
	function getTitle(){
		return "Gallery Page";
	}

	// in order for us to access the stored data in the $_SESSION across different pages, we jsut initialize the session with the session start function
	// syntax: session_start()

	// once we have initialized the session. we can then freely access all the session variables and its values stored in the current session and use it as we please.


	// if($_SESSION['email'] == 'admin@email.com'){
	// 	echo "Hello admin";
	// }
	// var_dump($_SESSION['user']['role_id']);
?>

<div id="body" class="container">
	<h2 class="text-center">
		Products Dashboard
	</h2>

	<div class="row">
		<div class="col-md-12">
			

		
			<h2>Sort By: </h2>
			<ul class="list-group border my-5 text-center">
				
				<li class="list-group-item">
					<a href="../controllers/sort.php?sort=asc">
						Price(Lowest to Highest)
					</a>
				</li>

				<li class="list-group-item">
					<a href="../controllers/sort.php?sort=desc">
						Price(Highest to Lowest)
					</a>
				</li>
				<li class="list-group-item">
					<a href="./cart.php">
						View Cart
					</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="row">
		

		<?php if (isset($_SESSION['user'])): ?>
			<?php if ($_SESSION['user']['role_id'] == 1): ?>

			<div class="col-md-4" >
				<div class="card">
					<a href="./new_product.php" class="waves-effect waves-light"><img src="../assets/images/add.png" class="card-img-top"></a>

					<div class="card-body">
						<h5 class="card-title">Add Product</h5>
						
					</div>
				</div>
			</div>

			<?php endif ?>
		<?php endif ?>

		
		<?php
	        $product_query = "SELECT * FROM items";
	        // var_dump($product_query);
	        // var_dump($_SESSION['sort']);

	        if (isset($_SESSION['sort'])) {
	        	# code...
	        	$product_query .= $_SESSION['sort'];
	        }

	        $product_array = mysqli_query($conn, $product_query);
	        // var_dump($products_array);

	        foreach($product_array as $product){
	        // var_dump($product['image']);


	    ?>
		<div class="col-md-4">
			<div class="card">
				<form action="./product_page.php" method="GET"> 
					<img src="<?= $product['image'] ?>" class="card-img-top" >
					<div class="card-body">
						<h5 class="card-title"><?= $product['name'] ?></h5>
						<p class="card-text">PHP <?= $product['price'] ?></p>
						<p class="card-text"><?= $product['description'] ?></p>


						<input value="<?= $product['image'] ?>" name="image" >
						<input value="<?= $product['name'] ?>" name="name">
						<input value="<?= $product['price'] ?>" name="price">
						<input value="<?= $product['description'] ?>" name="description">
					
						<button class="btn btn-secondary">View Item</button>
					</div>
				</form>

				<div class="card-footer">
					<form action="../controllers/update_cart.php" method="POST">
						<input type="number" class="form-control d-block" min="1" value="1" name="item_quantity">
						<input value="<?= $product['name'] ?>" name="name">
						<input value="<?= $product['price'] ?>" name="price">
						<input type="hidden" name="item_id" value="<?= $product['id'] ?>">
						<button class="btn-primary btn btn-block add-to-cart">Add To Cart</button>
						<?php if ($_SESSION['user']['role_id'] == 1): ?>
							<a href="../controllers/delete_item.php?id=<?= $product['id']; ?> " class="btn btn-outline-danger btn-block mt-3 delete">Delete Item</a>
							<a href="./edit_form.php?id=<?= $product['id']; ?>" class="btn btn-outline-danger btn-block mt-3 delete">Update Item</a>
						<?php endif ?>
					</form>
				</div>

			</div>

			
		</div>


		<?php 
			}
		?>
		
	</div>
</div>

<script type="text/javascript">
	$('input').hide();
</script>





<?php 
	require_once '../partials/footer.php';
?>