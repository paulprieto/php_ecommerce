<?php 
	session_start();
  // var_dump($_SESSION);
  // var_dump($_SESSION['user']['firstname']);
  // var_dump($_SESSION['user']['email']);
	// var_dump($_SESSION);

  require_once "../controllers/connection.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

	<title> STORESTORAN |<?php echo getTitle(); ?> </title>
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- materializecss -->
	<!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

	<!-- animate css -->
	

	<!-- fontawesome icons -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!-- google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Oswald|Source+Sans+Pro&display=swap" rel="stylesheet">
 



	<!-- external css -->
	<link rel="stylesheet" href="../assets/css/style.css">

  <!-- external JS -->
	<script type="text/javascript" src="../assets/js/script.js"></script>

</head>
<body>
	<nav>
    	<div class="nav-wrapper">
      		<a href="#" class="brand-logo center">STORESTORAN</a>
      		<ul id="nav-mobile" class="left hide-on-med-and-down">

      			<?php 
      				// isset() function checks if a variable or an element has been assigned a value.
      				// if session email has no assigned value/has not been set. display register and login in the navbar
      				// else, display the logout button

      				// var_dump($_SESSION);
      				if (isset($_SESSION['user']['firstname'])) {
      					# code...
      					$me = $_SESSION['user']['firstname'];
      				}
      				

      				if (!isset($_SESSION['user'])) {
      					# code...
      					// when no one is logged in.
      					echo "
      						<li><a href='./home.php'>Home</a></li>
                  <li><a href='./gallery.php'>Gallery</a></li>
      						<li><a href='./login.php'>Login</a></li>

      						<li><a href='./register.php'>Register</a></li>
      						<li><a href='#'>Hello Guest!</a></li>
      					";
      				}
      				else {
      					// someone is logged in 
      					echo "
      						<li><a href='./home.php'>Home</a></li>
                  <li><a href='./gallery.php'>Gallery</a></li>
			        		<li><a href='./logout.php'>logout</a></li>
			        		<li><a href='#'>Welcome $me!</a></li>
      					";
      				}
      			?>

        		<!-- <li><a href="./home.php">Home</a></li>
        		<li><a href="./register.php">Register</a></li>
        		<li><a href="./login.php">Login</a></li> -->
      		</ul>

          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="./gallery.php"><i class="fas fa-store"></i> Shop</a></li>
            <li><a href="./cart.php"><i class="fas fa-shopping-cart"></i> My Cart</a></li>
            

          </ul>
    	</div>
  </nav>



	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>